<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controllers functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controllers class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controllers class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controllers/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controllers and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controllers and method URI segments.
|
| Examples:	my-controllers/index	-> my_controller/index
|		my-controllers/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'page/page/home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['about']='page/page/about';
$route['contact']='page/page/contact';
$route['portfolio']='page/page/portfolio';
$route['products']='page/page/products';
$route['services']='page/page/services';
$route['admin']='page/page/admin';
$route['register']='user/user/register';
$route['login']='user/user/loginForm';
$route['register/store']='user/user/register_user';
$route['login/store']='user/user/login_user';
$route['verified/(:any)']='user/user/verified';
$route['page/create']='page/page/pageCreate';
$route['page/store']='page/page/pageStore';
$route['page/list']='page/page/pageList';
$route['page/edit/(:any)']='page/page/pageEdit';
$route['page/update/(:any)']='page/page/pageUpdate';
$route['page/delete/(:any)']='page/page/delete';
$route['slider/create']='slider/slider/sliderCreate';
$route['slider/store']='slider/slider/sliderStore';
$route['slider/list']='slider/slider/sliderList';
$route['slider/edit/(:any)']='slider/slider/sliderEdit';
$route['slider/update/(:any)']='slider/slider/sliderUpdate';
$route['slider/delete/(:any)']='slider/slider/delete';

$route['category/create']='category/category/categoryCreate';
$route['category/store']='category/category/categoryStore';
$route['category/list']='category/category/categoryList';
$route['category/edit/(:any)']='category/category/categoryEdit';
$route['category/update/(:any)']='category/category/categoryUpdate';
$route['category/delete/(:any)']='category/category/delete';
$route['product/create']='product/product/productCreate';
$route['product/store']='product/product/productStore';
$route['product/list']='product/product/productList';
$route['sub/create']='subcategory/subcategory/subCreate';
$route['sub/store']='subcategory/subcategory/subStore';
$route['sub/list']='subcategory/subcategory/subCategoryList';
$route['findsubcategory/(:any)']='product/product/findSubcategory';

