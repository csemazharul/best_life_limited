<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Basic Page Needs
	================================================== -->
	<meta charset="utf-8">
	<!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Home | Best Life Limited </title>
	<!--     <meta name="description" content="BEST LIFE LTD. is a clean, modern, and fully responsive Html Template. it is designed for corporate, finacial, insurance, agency, businesses or any type of person or business who wants to showcase their work, services and professional way.">
		<meta name="keywords" content="business, clean, company, corporate, gallery, huge, lightbox, modern, multipurpose, html template, portfolio, rometheme, startup">
		<meta name="author" content="rometheme.net">
		 -->
	<!-- ==============================================
	Favicons
	=============================================== -->
	<link rel="shortcut icon" href="<?php echo base_url(); ?>/ui/frontend/images/favicon.png">
	<link rel="apple-touch-icon" href="<?php echo base_url(); ?>/ui/frontend/images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>/ui/frontend/images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>/ui/frontend/images/apple-touch-icon-114x114.png">

	<!-- ==============================================
	CSS VENDOR
	=============================================== -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/ui/frontend/css/vendor/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/ui/frontend/css/vendor/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/ui/frontend/css/vendor/owl.carousel.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/ui/frontend/css/vendor/owl.theme.default.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/ui/frontend/css/vendor/magnific-popup.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/ui/frontend/css/vendor/animate.min.css">

	<!-- ==============================================
	Custom Stylesheet
	=============================================== -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/ui/frontend/css/style.css"/>

	<script src="<?php echo base_url(); ?>/ui/frontend/js/vendor/modernizr.min.js"></script>

</head>

<body data-spy="scroll" data-target="#navbar-example">

<!-- LOAD PAGE -->
<div class="animationload">
	<div class="loader"></div>
</div>

<!-- BACK TO TOP SECTION -->
<a href="#0" class="cd-top cd-is-visible cd-fade-out">Top</a>

<!-- HEADER -->
<div class="header header-1">

	<!-- TOPBAR -->
	<div class="topbar d-none d-md-block">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-sm-4 col-md-2 col-lg-4">
					<p class="mb-0"><em>A Big Market..</em></p>
				</div>
				<div class="col-sm-8 col-md-10 col-lg-8">
					<div class="info pull-right">
						<div class="info-item">
							<i class="fa fa-envelope-o"></i>Mail : support@bll.com
						</div>
						<div class="info-item">
							<i class="fa fa-phone"></i>Call Us : +880
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- NAVBAR SECTION -->
	<div class="navbar-main">
		<div class="container">
			<nav id="navbar-main" class="navbar navbar-expand-lg">
				<a class="navbar-brand" href="">
					<img style="margin-top: 6px; height: 80px;width: 280px;" src="<?php echo base_url(); ?>/ui/frontend/images/logo_main.png" alt="" />
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarNavDropdown">
					<ul class="navbar-nav ml-auto">
						<li class="nav-item">
							<a class="nav-link" href="<?php echo base_url(); ?>">HOME</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="<?php echo base_url(); ?>about">About</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="<?php echo base_url(); ?>services">SERVICES</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="<?php echo base_url(); ?>portfolio"> PORTFOLIO</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="<?php echo base_url(); ?>products"> OUR PRODUCTS</a>
						</li>

						<li class="nav-item">
							<a class="nav-link" href="<?php echo base_url(); ?>contact">CONTACT US</a>
						</li>
					</ul>
				</div>
			</nav> <!-- -->
		</div>
	</div>
</div>
