<!-- FOOTER SECTION -->
<div class="footer">
	<div class="content-wrap pb-0">
		<div class="container">

			<div class="row">
				<div class="col-sm-6 col-md-6 col-lg-3">
					<div class="footer-item">
						<img width="230px" height="60px" src="images/logo_main 2.png" alt="logo bottom" class="logo-bottom">

						<p>BEST LIFE LTD. is a clean, modern, and fully responsive Html Template. it is designed for corporate, finacial, insurance, agency, businesses or any type of person or business who wants to showcase their work, services and professional way.</p>
						<div class="sosmed-icon d-inline-flex">
							<a href="#"><i class="fa fa-facebook"></i></a>
							<a href="#"><i class="fa fa-twitter"></i></a>
							<a href="#"><i class="fa fa-instagram"></i></a>
							<a href="#"><i class="fa fa-pinterest"></i></a>
							<a href="#"><i class="fa fa-linkedin"></i></a>
						</div>
					</div>
				</div>

				<div class="col-sm-6 col-md-6 col-lg-3">
					<div class="footer-item">
						<div class="footer-title">
							LATEST NEWS
						</div>

						<ul class="recent-post">
							<li><a href="#" title="">The Best in dolor sit amet consectetur adipisicing elit sed</a>
								<span class="date"><i class="fa fa-clock-o"></i> June 16, 2017</span></li><li><a href="#" title="">The Best in dolor sit amet consectetur adipisicing elit sed</a>
								<span class="date"><i class="fa fa-clock-o"></i> June 16, 2017</span></li>
						</ul>

					</div>
				</div>

				<div class="col-sm-6 col-md-6 col-lg-3">
					<div class="footer-item">
						<div class="footer-title">
							USEFUL LINKS
						</div>
						<ul class="list">
							<li><a href="#" title="About Us">About Us</a></li>
							<li><a href="#" title="Corporate Profile">Corporate Profile</a></li>
							<li><a href="#" title="Our Team">Our Team</a></li>
							<li><a href="#" title="Portfolio">Portfolio</a></li>
							<li><a href="#" title="Our Office">Our Office</a></li>
						</ul>
					</div>
				</div>

				<div class="col-sm-6 col-md-6 col-lg-3">
					<div class="footer-item">
						<div class="footer-title">
							CONTACT INFO
						</div>

						<ul class="list-info">
							<li>
								<div class="info-icon">
									<span class="fa fa-map-marker"></span>
								</div>
								<div class="info-text">99 S.t Jomblo Park Pekanbaru 28292. Indonesia</div> </li>
							<li>
								<div class="info-icon">
									<span class="fa fa-phone"></span>
								</div>
								<div class="info-text">(0761) 654-123987</div>
							</li>
							<li>
								<div class="info-icon">
									<span class="fa fa-envelope"></span>
								</div>
								<div class="info-text">info@yoursite.com</div>
							</li>
							<li>
								<div class="info-icon">
									<span class="fa fa-clock-o"></span>
								</div>
								<div class="info-text">Mon - Sat 09:00 - 17:00</div>
							</li>
						</ul>

					</div>
				</div>

			</div>
		</div>
	</div>

	<div class="fcopy">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<p class="ftex">Copyright 2019 &copy; <span style="margin-right: 10px" class="color-primary">BEST LIFE LTD. </span> Developed by <span class="color-primary" ><a target="_blank" href="http://rcreation-bd.com/">R-Creation</a></span></p>
				</div>
			</div>
		</div>
	</div>

</div>


<!-- JS VENDOR -->
<script src="<?php echo base_url(); ?>/ui/frontend/js/vendor/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>/ui/frontend/js/vendor/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>/ui/frontend/js/vendor/owl.carousel.js"></script>
<script src="<?php echo base_url(); ?>/ui/frontend/js/vendor/jquery.magnific-popup.min.js"></script>
<script src="<?php echo base_url(); ?>/ui/frontend/js/vendor/isotope.pkgd.min.js"></script>
<script src="<?php echo base_url(); ?>/ui/frontend/js/vendor/imagesloaded.pkgd.min.js"></script>

<!-- SENDMAIL -->
<script src="<?php echo base_url(); ?>/ui/frontend/js/vendor/validator.min.js"></script>
<script src="<?php echo base_url(); ?>/ui/frontend/js/vendor/form-scripts.js"></script>

<!-- GOOGLEMAP -->
<script src="<?php echo base_url(); ?>/ui/frontend/js/googlemap-setting.js"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA-CE0deH3Jhj6GN4YvdCFZS7DpbXexzGU&callback=initMap"> </script>

<script src="<?php echo base_url(); ?>/ui/frontend/js/script.js"></script>

</body>
</html>
