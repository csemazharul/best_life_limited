<?php
$this->load->view('frontend/layout/header');
?>


<!-- BANNER -->
<div class="section banner-page" data-background="images/slider/1920x300-1.jpg">
	<div class="content-wrap pos-relative">
		<div class="d-flex justify-content-center bd-highlight mb-3">
			<div class="title-page">CONTACT</div>
		</div>
		<div class="d-flex justify-content-center bd-highlight mb-3">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb ">
					<li class="breadcrumb-item"><a href="index.html">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page">Contact</li>
				</ol>
			</nav>
		</div>
	</div>
</div>

<!-- CONTENT -->
<div class="section">
	<div class="content-wrap">
		<div class="container">
			<div class="row">

				<div class="col-sm-12 col-md-12 col-lg-8">

					<h2 class="section-heading text-left mb-5">
						SEND A LETTER
					</h2>
					<p class="subheading text-left">We provide high standar clean website for your business solutions</p>
					<form action="#" class="form-contact" id="contactForm">
						<div class="row">
							<div class="col-sm-6 col-md-6">
								<div class="form-group">
									<input type="text" class="form-control" id="p_name" placeholder="Enter Name" required="">
									<div class="help-block with-errors"></div>
								</div>
							</div>
							<div class="col-sm-6 col-md-6">
								<div class="form-group">
									<input type="email" class="form-control" id="p_email" placeholder="Enter Email" required="">
									<div class="help-block with-errors"></div>
								</div>
							</div>
							<div class="col-sm-6 col-md-6">
								<div class="form-group">
									<input type="text" class="form-control" id="p_subject" placeholder="Subject">
									<div class="help-block with-errors"></div>
								</div>
							</div>
							<div class="col-sm-6 col-md-6">
								<div class="form-group">
									<input type="text" class="form-control" id="p_phone" placeholder="Enter Phone Number">
									<div class="help-block with-errors"></div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<textarea id="p_message" class="form-control" rows="6" placeholder="Enter Your Message"></textarea>
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group">
							<div class="text-left">
								<div id="success"></div>
								<button type="submit" class="btn btn-primary">SEND MESSAGE</button>
							</div>
						</div>
					</form>


				</div>

				<div class="col-sm-12 col-md-12 col-lg-4">
					<h2 class="section-heading text-left mb-5">
						CONTACT DETAILS
					</h2>
					<!-- Item 1 -->
					<div class="rs-icon-info-2">
						<div class="info-icon">
							<i class="fa fa-map-marker"></i>
						</div>
						<div class="body-text">
							<h4>Address</h4>
							<p>99 S.t Jomblo Park Pekanbaru 28292. Indonesia</p>
						</div>
					</div>
					<!-- Item 2 -->
					<div class="rs-icon-info-2">
						<div class="info-icon">
							<i class="fa fa-phone"></i>
						</div>
						<div class="body-text">
							<h4>Phone</h4>
							<p>+62 7100 1234 <br>+62 7100 1235</p>
						</div>
					</div>
					<!-- Item 3 -->
					<div class="rs-icon-info-2">
						<div class="info-icon">
							<i class="fa fa-envelope"></i>
						</div>
						<div class="body-text">
							<h4>Email</h4>
							<p>support@bll.com<br>message@bll.com</p>
						</div>
					</div>

				</div>

			</div>
		</div>
	</div>

	<!-- MAPS -->
	<div class="maps-wraper">
		<div id="cd-zoom-in"></div>
		<div id="cd-zoom-out"></div>
		<div id="maps" class="maps" data-lat="-7.452278" data-lng="112.708992" data-marker="images/cd-icon-location.png">
		</div>
	</div>

</div>

<!-- CTA -->
<div class="section bg-primary">
	<div class="content-wrap py-5">
		<div class="container">

			<div class="row align-items-center">
				<div class="col-sm-12 col-md-12">
					<div class="cta-1">
						<div class="body-text text-white mb-3">
							<h3 class="my-1">Grow Up Your Business With BEST LIFE LTD.</h3>
							<p class="uk18 mb-0">We provide high standar clean website for your business solutions</p>
						</div>
						<div class="body-action mt-3">
							<a href="#" class="btn btn-secondary">PURCHASE NOW</a>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

<?php
$this->load->view('frontend/layout/footer');
?>
