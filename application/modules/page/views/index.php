<?php
$this->load->view('frontend/layout/header');
?>
<!-- BANNER -->

<div class="banner">
	<div class="owl-carousel owl-theme full-screen">
		<!-- Item 1 -->
		<?php
		foreach($sliders as $slider){

		?>
			<div class="item">
				<img src="<?php echo base_url(); ?>upload/images/<?php echo $slider->picture ?>" alt="Slider">
				<div class="container d-flex align-items-center h-center">
					<div class="wrap-caption">
						<h1 class="caption-heading"><?php echo $slider->title ?></h1>
						<p class="uk24"><?php echo $slider->short_description ?></p>
						<a href="#" class="btn btn-primary">DISCOVER MORE</a> &nbsp;
						<a href="#" class="btn btn-primary">PURCHASE NOW</a>
					</div>
				</div>
			</div>
		<?php
		}

		?>

<!--		 Item 2 -->
<!--		<div class="item">-->
<!--			<img src="--><?php //echo base_url(); ?><!--/ui/frontend/images/slider/1920x900-1.jpg" alt="Slider">-->
<!--			<div class="container d-flex align-items-center h-center">-->
<!--				<div class="wrap-caption">-->
<!--					<h1 class="caption-heading">THINK BUSINESS THINK BEST LIFE LTD.</h1>-->
<!--					<p class="uk24">Be Creative. Innovative. & Inspirative</p>-->
<!--					<a href="#" class="btn btn-primary">DISCOVER MORE</a> &nbsp;-->
<!--					<a href="#" class="btn btn-primary">PURCHASE NOW</a>-->
<!--				</div>-->
<!--			</div>-->
<!--		</div>-->
<!---->

<!---->
<!--		<div class="item">-->
<!--			<img src="--><?php //echo base_url(); ?><!--/ui/frontend/images/slider/1920x900-3.jpg" alt="Slider">-->
<!--			<div class="container d-flex align-items-center h-center">-->
<!--				<div class="wrap-caption">-->
<!--					<h1 class="caption-heading">THINK BUSINESS THINK BEST LIFE LTD.</h1>-->
<!--					<p class="uk24">Be Creative. Innovative. & Inspirative</p>-->
<!--					<a href="#" class="btn btn-primary">DISCOVER MORE</a> &nbsp;-->
<!--					<a href="#" class="btn btn-primary">PURCHASE NOW</a>-->
<!--				</div>-->
<!--			</div>-->
<!--		</div>-->

	</div>
	<div class="custom-nav owl-nav"></div>
</div>

<!-- CTA -->
<!-- 	<div class="section bg-primary">
		<div class="content-wrap py-5">
			<div class="container">

				<div class="row align-items-center">
					<div class="col-sm-12 col-md-12">
						<div class="cta-1">
			              	<div class="body-text text-white mb-3">
			                	<h3 class="my-1">Grow Up Your Business With BEST LIFE LTD.</h3>
			                	<p class="uk18 mb-0">We provide high standar clean website for your business solutions</p>
			              	</div>
			              	<div class="body-action mt-3">
			                	<a href="#" class="btn btn-secondary">PURCHASE NOW</a>
			              	</div>
			            </div>
					</div>
				</div>

			</div>
		</div>
	</div> -->

<!-- ABOUT -->
<div class="section">
	<div class="content-wrap">
		<div class="container">

			<div class="row">

				<div class="col-sm-12 col-md-12">
					<h2 class="section-heading text-center">
						Hello! Welcome to BEST LIFE LTD. Business
					</h2>
					<p class="subheading text-center mb-5">Awesome features we offer exclusive only</p>
				</div>

			</div>

			<div class="row">
				<!-- Item 1 -->
				<div class="col-sm-12 col-md-12 col-lg-4 mb-2">
					<div class="box-icon-1 text-center">
						<div class="icon">
							<i class="fa fa-paint-brush"></i>
						</div>
						<div class="body-content">
							<h4>Low profit margin</h4>
							<p>Most of the wholesalers do their business for low profit margin.</p>
						</div>
					</div>
				</div>
				<!-- Item 2 -->
				<div class="col-sm-12 col-md-12 col-lg-4 mb-2">
					<div class="box-icon-1 text-center">
						<div class="icon">
							<i class="fa fa-gears"></i>
						</div>
						<div class="body-content">
							<h4>Sales on credit</h4>
							<p>Generally, wholesalers sell goods to retailers on credit.</p>
						</div>
					</div>
				</div>
				<!-- Item 3 -->
				<div class="col-sm-12 col-md-12 col-lg-4 mb-2">
					<div class="box-icon-1 text-center">
						<div class="icon">
							<i class="fa fa-rocket"></i>
						</div>
						<div class="body-content">
							<h4> Bulk quantity</h4>
							<p>Bulk buying is another feature of wholesaling. Wholesalers buy goods in bulk quantity from producers.</p>
						</div>
					</div>
				</div>

			</div>

		</div>
	</div>
</div>

<!-- CTA -->
<div class="section bgi-cover-center cta" data-background="<?php echo base_url(); ?>/ui/frontend/images/slider/1920x900-4.jpg">
	<div class="content-wrap">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-sm-12 col-md-12 col-lg-8 offset-lg-2">
					<div class="text-center">
						<h2 class="text-white">BEST LIFE LTD. PRESENTATION</h2>
						<p class="uk18 text-white">Click this video to explore more</p>
						<a href="https://www.youtube.com/watch?v=vNDrLjOmUY4" class="popup-youtube btn-video"><i class="fa fa-play fa-2x"></i></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- WHO WE ARE -->
<div class="section bg-gray-light">
	<div class="content-wrap">
		<div class="container">


			<div class="row">
				<div class="col-sm-12 col-md-12 col-lg-6">
					<h2 class="section-heading text-left">
						WHO WE ARE?
					</h2>

					<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium dolore mque laudantium, totam rem aperiam, eaque ipsa quae ab illo invent.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
					<a href="#" class="btn btn-primary">READ MORE</a>
					<div class="spacer-30"></div>
				</div>
				<div class="col-sm-12 col-md-12 col-lg-6">

					<div id="whoweare" class="whoweare owl-carousel owl-theme" data-background="<?php echo base_url(); ?>/ui/frontend/images/laptop.png">
						<!-- Item 1 -->
						<div class="item">
							<img src="<?php echo base_url(); ?>/ui/frontend/images/600x400-.jpg" alt="">
						</div>
						<!-- Item 2 -->
						<div class="item">
							<img src="<?php echo base_url(); ?>/ui/frontend/images/600x400-2.jpg" alt="">
						</div>
						<!-- Item 3 -->
						<div class="item">
							<img src="<?php echo base_url(); ?>/ui/frontend/images/600x400-4.jpg" alt="">
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>

<!-- PORTFOLIO -->
<div class="section">
	<div class="content-wrap">
		<div class="container">

			<div class="row mb-5">
				<div class="col-sm-12 col-md-12">
					<h2 class="section-heading text-center no-after mb-3">
						OUR PRODUCTS
					</h2>
				</div>
			</div>

			<div class="row popup-gallery">
				<!-- Item 1 -->
				<div class="col-sm-12 col-md-6 col-lg-4 mb-4">
					<div class="rs-box-project">
						<div class="media-box">
							<a href="<?php echo base_url(); ?>/ui/frontend/images/600x500-6.jpg" title="Gallery #1">
								<img src="<?php echo base_url(); ?>/ui/frontend/images/600x500-6.jpg" alt="" class="img-fluid">
								<div class="project-info">
									<div class="project-icon">
										<span class="fa fa-search"></span>
									</div>
								</div>
							</a>
						</div>
						<div class="body">
							<div class="title">Notebook Mockup</div>
							<div class="category">Design</div>

						</div>
					</div>
				</div>
				<!-- Item 2 -->
				<div class="col-sm-12 col-md-6 col-lg-4 mb-4">
					<div class="rs-box-project">
						<div class="media-box">
							<a href="<?php echo base_url(); ?>/ui/frontend/images/600x500-5.jpg" title="Gallery #2">
								<img src="<?php echo base_url(); ?>/ui/frontend/images/600x500-5.jpg" alt="" class="img-fluid">
								<div class="project-info">
									<div class="project-icon">
										<span class="fa fa-search"></span>
									</div>
								</div>
							</a>
						</div>
						<div class="body">
							<div class="title">Paper Hot Cup</div>
							<div class="category">Design</div>

						</div>
					</div>
				</div>
				<!-- Item 3 -->
				<div class="col-sm-12 col-md-6 col-lg-4 mb-4">
					<div class="rs-box-project">
						<div class="media-box">
							<a href="<?php echo base_url(); ?>/ui/frontend/images/600x500-.jpg" title="Gallery #3">
								<img src="<?php echo base_url(); ?>/ui/frontend/images/600x500-.jpg" alt="" class="img-fluid">
								<div class="project-info">
									<div class="project-icon">
										<span class="fa fa-search"></span>
									</div>
								</div>
							</a>
						</div>
						<div class="body">
							<div class="title">Jamu Bottle</div>
							<div class="category">Design</div>

						</div>
					</div>
				</div>
				<!-- Item 4 -->
				<div class="col-sm-12 col-md-6 col-lg-4 mb-4">
					<div class="rs-box-project">
						<div class="media-box">
							<a href="<?php echo base_url(); ?>/ui/frontend/images/600x500-2.jpg" title="Gallery #4">
								<img src="<?php echo base_url(); ?>/ui/frontend/images/600x500-2.jpg" alt="" class="img-fluid">
								<div class="project-info">
									<div class="project-icon">
										<span class="fa fa-search"></span>
									</div>
								</div>
							</a>
						</div>
						<div class="body">
							<div class="title">Papper Bag</div>
							<div class="category">Design</div>

						</div>
					</div>
				</div>
				<!-- Item 5 -->
				<div class="col-sm-12 col-md-6 col-lg-4 mb-4">
					<div class="rs-box-project">
						<div class="media-box">
							<a href="<?php echo base_url(); ?>/ui/frontend/images/600x500-3.jpg" title="Gallery #5">
								<img src="<?php echo base_url(); ?>/ui/frontend/images/600x500-3.jpg" alt="" class="img-fluid">
								<div class="project-info">
									<div class="project-icon">
										<span class="fa fa-search"></span>
									</div>
								</div>
							</a>
						</div>
						<div class="body">
							<div class="title">Hanging T-Shirt</div>
							<div class="category">Design</div>

						</div>
					</div>
				</div>
				<!-- Item 6 -->
				<div class="col-sm-12 col-md-6 col-lg-4 mb-4">
					<div class="rs-box-project">
						<div class="media-box">
							<a href="<?php echo base_url(); ?>/ui/frontend/images/600x500-4.jpg" title="Gallery #6">
								<img src="<?php echo base_url(); ?>/ui/frontend/images/600x500-4.jpg" alt="" class="img-fluid">
								<div class="project-info">
									<div class="project-icon">
										<span class="fa fa-search"></span>
									</div>
								</div>
							</a>
						</div>
						<div class="body">
							<div class="title">Notebook Mockup Resource</div>
							<div class="category">Design</div>

						</div>
					</div>
				</div>
			</div>

			<div class="text-center mt-5">
				<a href="our_product.html" class="btn btn-primary">SEE MORE</a>
			</div>

		</div>
	</div>
</div>

<!-- FUN FACT -->
<!-- 	<div class="section bg-secondary">
		<div class="content-wrap">
			<div class="container">

				<div class="row"> -->

<!-- Item 1 -->
<!-- 	<div class="col-sm-6 col-md-6 col-lg-3">
		<div class="rs-icon-funfact-2">
			<div class="icon">
				<i class="fa fa-briefcase"></i>
			</div>
			<div class="body-content">
				<h2>1200</h2>
				<p>PROJECTS</p>
			</div>
		</div>
	</div> -->

<!-- Item 2 -->
<!-- <div class="col-sm-6 col-md-6 col-lg-3">
	<div class="rs-icon-funfact-2">
		<div class="icon">
			<i class="fa fa-thumbs-up"></i>
		</div>
		<div class="body-content">
			<h2>25000</h2>
			<p>FOLLOW & LIKE</p>
		</div>
	</div>
</div> -->

<!-- Item 3 -->
<!-- 	<div class="col-sm-6 col-md-6 col-lg-3">
		<div class="rs-icon-funfact-2">
			<div class="icon">
				<i class="fa fa-coffee"></i>
			</div>
			<div class="body-content">
				<h2>6000</h2>
				<p>CUP OF COFFEE</p>
			</div>
		</div>
	</div>
-->
<!-- Item 4 -->
<!-- 	<div class="col-sm-6 col-md-6 col-lg-3">
		<div class="rs-icon-funfact-2">
			<div class="icon">
				<i class="fa fa-users"></i>
			</div>
			<div class="body-content">
				<h2>800</h2>
				<p>CLIENTS</p>
			</div>
		</div>
	</div> -->

<!-- 			</div>
		</div>
	</div>
</div>
-->
<!-- AWESOME SERVICE -->
<div class="section section-border">
	<div class="content-wrap">
		<div class="container">

			<div class="row">

				<div class="col-sm-12 col-md-12 col-lg-4">
					<h2 class="section-heading text-left">
						AWESOME SERVICE
					</h2>

					<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium dolore mque laudantium, totam rem aperiam, eaque ipsa quae ab illo invent.</p>

					<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium dolore mque laudantium, totam rem aperiam, eaque ipsa quae ab illo invent.</p>

					<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium dolore mque laudantium, totam rem aperiam, eaque ipsa quae ab illo invent.</p>
					<!-- <div class="rs-progress mb-3">
						  <div class="name">Development</div>
							<div class="persen">80%</div>
						  <div class="progress">
						  <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
					</div>
					<div class="rs-progress mb-3">
						  <div class="name">HTML</div>
							<div class="persen">90%</div>
						  <div class="progress">
						  <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
					</div>
					<div class="rs-progress mb-3">
						  <div class="name">Marketing</div>
							<div class="persen">70%</div>
						  <div class="progress">
						  <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
					</div>
					<div class="rs-progress mb-3">
						  <div class="name">Adobe Apps</div>
							<div class="persen">80%</div>
						  <div class="progress">
						  <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
					</div> -->


				</div>

				<div class="col-sm-12 col-md-12 col-lg-8">

					<img src="<?php echo base_url(); ?>/ui/frontend/images/900x600.jpg" alt="" class="img-fluid shadow-lg mb-3">

				</div>

			</div>

		</div>
	</div>
</div>

<!-- OUR TESTIMONIALS -->
<!-- 	<div class="section bgi-cover-center" data-background="<?php echo base_url(); ?>/ui/frontend/images/slider/1920x900-2.jpg">
		<div class="content-wrap">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<h2 class="section-heading text-center text-white mb-5">
							CLIENTS SAY
						</h2>
					</div>
				</div>
				<div class="row">
					
					<div class="col-sm-12 col-md-12 col-lg-8 offset-lg-2">
						<div id="testimonial" class="owl-carousel owl-theme owl-light">
							
							<div class="item">
								<div class="rs-testimonial-3">
									<div class="quote-box">
										<blockquote>
										 Teritatis et quasi architecto. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium dolore mque laudantium, totam rem aperiam
										</blockquote>
										<div class="media">
											<img src="<?php echo base_url(); ?>/ui/frontend/images/dummy-img-400x400.jpg" alt="" class="rounded-circle">
										</div>
										<p class="quote-name">
											Johnathan Doel <span>CEO Rometheme</span>
										</p>                        
									</div>
								</div>
							</div>
				
							<div class="item">
								<div class="rs-testimonial-3">
									<div class="quote-box">
										<blockquote>
										 Teritatis et quasi architecto. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium dolore mque laudantium, totam rem aperiam
										</blockquote>
										<div class="media">
											<img src="<?php echo base_url(); ?>/ui/frontend/images/dummy-img-400x400.jpg" alt="" class="rounded-circle">
										</div>
										<p class="quote-name">
											Linda Doel <span>CEO Bukakreasi</span>
										</p>                        
									</div>
								</div>
							</div>
						
							<div class="item">
								<div class="rs-testimonial-3">
									<div class="quote-box">
										<blockquote>
										 Teritatis et quasi architecto. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium dolore mque laudantium, totam rem aperiam
										</blockquote>
										<div class="media">
											<img src="<?php echo base_url(); ?>/ui/frontend/images/dummy-img-400x400.jpg" alt="" class="rounded-circle">
										</div>
										<p class="quote-name">
											Johny Doel <span>CEO abc.xyz</span>
										</p>                        
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
	 -->
<!-- LATEST NEWS -->
<div class="section">
	<div class="content-wrap">
		<div class="container">

			<div class="row">
				<div class="col-sm-12 col-md-12">
					<h2 class="section-heading text-center text-primary no-after mb-5">
						LATEST NEWS
					</h2>
					<p class="subheading text-center">We provide high standar clean website for your business solutions</p>
				</div>
			</div>

			<div class="row mt-4">

				<!-- Item 1 -->
				<div class="col-sm-12 col-md-12 col-lg-4">
					<div class="rs-news-1 mb-1">
						<div class="media-box">
							<div class="meta-date"><span>30</span>May</div>
							<a href="blog-single.html">
								<img src="<?php echo base_url(); ?>/ui/frontend/images/600x500-2.jpg" alt="" class="img-fluid">
							</a>
						</div>
						<div class="body-box">
							<div class="title"><a href="blog-single.html">TYPING NEW KEYBOARD</a></div>
							<p>Dignissimos ccusamus et iusto odio ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores....</p>
						</div>
					</div>
				</div>

				<!-- Item 2 -->
				<div class="col-sm-12 col-md-12 col-lg-4">
					<div class="rs-news-1 mb-1">
						<div class="media-box">
							<div class="meta-date"><span>04</span>Jun</div>
							<a href="blog-single.html">
								<img src="<?php echo base_url(); ?>/ui/frontend/images/600x500-3.jpg" alt="" class="img-fluid">
							</a>
						</div>
						<div class="body-box">
							<div class="title"><a href="blog-single.html">NEW HARDWARE SHOW UP</a></div>
							<p>Dignissimos ccusamus et iusto odio ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores....</p>
						</div>
					</div>
				</div>

				<!-- Item 3 -->
				<div class="col-sm-12 col-md-12 col-lg-4">
					<div class="rs-news-1 mb-1">
						<div class="media-box">
							<div class="meta-date"><span>16</span>Jun</div>
							<a href="blog-single.html">
								<img src="<?php echo base_url(); ?>/ui/frontend/images/600x500-4.jpg" alt="" class="img-fluid">
							</a>
						</div>
						<div class="body-box">
							<div class="title"><a href="blog-single.html">MOCK WITH WOOD TABLE</a></div>
							<p>Dignissimos ccusamus et iusto odio ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores....</p>
						</div>
					</div>
				</div>

			</div>

			<div class="row mt-4">
				<div class="col-sm-12 col-md-12 col-lg-12">
					<div class="text-center">
						<a href="#" class="btn btn-primary">MORE POST</a>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

<!-- OUR PARTNERS -->
<!-- 	<div class="section bg-secondary">
		<div class="content-wrap py-5">
			<div class="container">
				
				<div class="row">
					<div class="col-12">
						<h2 class="section-heading text-center text-white">
							OUR CLIENTS
						</h2>
					</div>
				</div>
				<div class="row gutter-5">
					<div class="col-6 col-md-4 col-lg-2">
						<a href="#"><img src="<?php echo base_url(); ?>/ui/frontend/images/client1w.png" alt="" class="img-fluid img-border"></a>
					</div>
					<div class="col-6 col-md-4 col-lg-2">
						<a href="#"><img src="<?php echo base_url(); ?>/ui/frontend/images/client2w.png" alt="" class="img-fluid img-border"></a>
					</div>
					<div class="col-6 col-md-4 col-lg-2">
						<a href="#"><img src="<?php echo base_url(); ?>/ui/frontend/images/client3w.png" alt="" class="img-fluid img-border"></a>
					</div>
					<div class="col-6 col-md-4 col-lg-2">
						<a href="#"><img src="<?php echo base_url(); ?>/ui/frontend/images/client4w.png" alt="" class="img-fluid img-border"></a>
					</div>
					<div class="col-6 col-md-4 col-lg-2">
						<a href="#"><img src="<?php echo base_url(); ?>/ui/frontend/images/client5w.png" alt="" class="img-fluid img-border"></a>
					</div>
					<div class="col-6 col-md-4 col-lg-2">
						<a href="#"><img src="<?php echo base_url(); ?>/ui/frontend/images/client6w.png" alt="" class="img-fluid img-border"></a>
					</div>

				</div>
			</div>
		</div>
	</div> -->

<!-- CTA -->
<!-- <div class="section bgi-cover-center bg-primary" data-background="<?php echo base_url(); ?>/ui/frontend/images/slider/1920x900-2.jpg">
	<div class="content-wrap">
		<div class="container">
			
			<div class="row">
				<div class="col-sm-12 col-md-12 col-lg-12">
					<div class="text-center text-white">
						<h3>SUBSCRIBE TO GET IN TOUCH</h3>
						<p class="uk18 mb-0">We provide high standar clean website for your business solutions</p>
						
							<form class="form-inline justify-content-center subscribe-form mt-5">
							  <div class="form-group mx-sm-3 mb-2">
								<label for="p_email" class="sr-only">Password</label>
								<input type="email" class="form-control" id="p_email" placeholder="Enter your email address">
							  </div>
							  <button type="submit" class="btn btn-secondary mb-2">Subscribe</button>
							</form>
						
					</div>
				</div>
			</div>

		</div>
	</div>
</div> -->

<?php
$this->load->view('frontend/layout/footer');
?>
