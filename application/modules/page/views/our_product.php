<?php
$this->load->view('frontend/layout/header');
?>



<!-- BANNER -->
<div class="section banner-page-2">
	<div class="content-wrap pos-relative">
		<div class="container">

			<div class="row">
				<div class="col-sm-12 col-md-12 col-lg-12">

					<div class="mb-3">
						<div class="title-page">OUR PRODCTS</div>
					</div>
					<div class="mb-3">
						<nav aria-label="breadcrumb">
							<ol class="breadcrumb ">
								<li class="breadcrumb-item"><a href="index.html">Home</a></li>
								<li class="breadcrumb-item active" aria-current="page">Our Products</li>
							</ol>
						</nav>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>

<!-- ABOUT -->
<div class="section">
	<div class="content-wrap">
		<div class="container">

			<div class="row">

				<aside class="sidebar-shop col-lg-3 ">
					<div class="sidebar-wrapper">


						<div class="widget">
							<h3 class="widget-title">
								<a data-toggle="collapse" href="#widget-body-1" role="button" aria-expanded="true" aria-controls="widget-body-1">Catagories</a>
							</h3>
							<nav id="nav_side">

								<ul>

									<!-- Menu tooggle on smaller screens  -->
									<!--     <li id="toggleMenu"> <a href="/sitemap.html"> <i class="fa fa-bars"></i> <i class="fa fa-times"></i>  -->

									<!-- As icons are being used to represent open/close,
											provide hidden text for screen readers -->
									<!--       <span id="toggleMenu-text">Toggle Navigation</span> </a> </li> -->

									<!-- Simple menu item without sub menu -->
									<!--     <li><a href="/">Level 1</a></li> -->

									<!--  Menu item with one sub menu -->
									<!--     <li> <a href="/sitemap.html">Level 2</a>
										  <ul>
											<li><a href="/">Example Link</a></li>
											<li><a href="/">Example Link</a></li>
										  </ul>
										</li> -->

									<!--  Menu item with two levels of sub menu -->
									<?php
									foreach($categories as $category){
									?>
										<li> <a class="hash_arrow" href="/sitemap.html"><?php echo $category->title?>
										</a>
										<ul>
											<li ><a href="/">Example Link</a></li>
											<li><a href="/">Example Link</a></li>
											<li class="hash_arrow"> <a href="/">Example Link</a>
												<ul>
													<li><a href="/">Example Link</a></li>
													<li><a href="/">Example Link</a></li>
													<li><a href="/">Example Link</a></li>
												</ul>
											</li>
										</ul>
									</li>
										<?php
									}
									?>


								</ul>
							</nav>
						</div>




						<div class="widget">
							<h3 class="widget-title">
								<a data-toggle="collapse" href="#widget-body-2" role="button" aria-expanded="true" aria-controls="widget-body-2">Price</a>
							</h3>

							<div class="collapse show" id="widget-body-2">
								<div class="widget-body">
									<div class="filter-content">

										<div class="form-row">
											<div class="form-group col-md-6">
												<label>Min</label>
												<input type="number" class="form-control" id="inputEmail4" placeholder="$0">
											</div>
											<div class="form-group col-md-6 text-right">
												<label>Max</label>
												<input type="number" class="form-control" placeholder="$1,0000">
											</div>
										</div>

									</div>
								</div><!-- End .widget-body -->
							</div><!-- End .collapse -->
						</div><!-- End .widget -->

						<div class="widget">
							<h3 class="widget-title">
								<a data-toggle="collapse" href="#widget-body-3" role="button" aria-expanded="true" aria-controls="widget-body-3">Size</a>
							</h3>

							<div class="collapse show" id="widget-body-3">
								<div class="widget-body">
									<div class="row">
										<div class="col-lg-6 col-md-6 col-sm-6 col-sx-6">
											<label class="container-cheeck-size">S
												<input type="checkbox" checked="checked">
												<span class="checkmark"></span>
											</label>
											<label class="container-cheeck-size">M
												<input type="checkbox">
												<span class="checkmark"></span>
											</label>
											<label class="container-cheeck-size">L
												<input type="checkbox">
												<span class="checkmark"></span>
											</label>


										</div>
										<div class="col-lg-6 col-md-6 col-sm-6 col-sx-6">
											<label class="container-cheeck-size">XL
												<input type="checkbox" checked="checked">
												<span class="checkmark"></span>
											</label>
											<label class="container-cheeck-size">2XL
												<input type="checkbox">
												<span class="checkmark"></span>
											</label>
											<label class="container-cheeck-size">3XL
												<input type="checkbox">
												<span class="checkmark"></span>
											</label>


										</div>
									</div>
									<!--  <ul class="config-size-list">
										  <li><a href="#">S</a></li>
										  <li class="active"><a href="#">M</a></li>
										  <li><a href="#">L</a></li>
										  <li><a href="#">XL</a></li>
										  <li><a href="#">2XL</a></li>
										  <li><a href="#">3XL</a></li>
									  </ul>  -->
								</div><!-- End .widget-body -->
							</div><!-- End .collapse -->
						</div><!-- End .widget -->

						<div class="widget">
							<h3 class="widget-title">
								<a data-toggle="collapse" href="#widget-body-4" role="button" aria-expanded="true" aria-controls="widget-body-4">Brands</a>
							</h3>

							<div class="collapse show" id="widget-body-4">
								<div class="widget-body">
									<ul class="cat-list">
										<li><a href="#">Adidas <span>18</span></a></li>
										<li><a href="#">Camel <span>22</span></a></li>
										<li><a href="#">Seiko <span>05</span></a></li>
										<li><a href="#">Samsung Galaxy <span>68</span></a></li>
										<li><a href="#">Sony <span>03</span></a></li>
									</ul>
								</div><!-- End .widget-body -->
							</div><!-- End .collapse -->
						</div><!-- End .widget -->

						<div class="widget">
							<h3 class="widget-title">
								<a data-toggle="collapse" href="#widget-body-6" role="button" aria-expanded="true" aria-controls="widget-body-6">Color</a>
							</h3>





							<ul class="widget-body-ul">
								<li><label class="container-cheeck">
										<input data-toggle="tooltip" data-placement="top" title="Red" type="checkbox" checked="checked">
										<span class="checkmark red"></span>
									</label></li>
								<li><label class="container-cheeck">
										<input type="checkbox">
										<span data-toggle="tooltip" data-placement="top" title="Blue" class="checkmark blue"></span>
									</label></li>
								<li><label class="container-cheeck">
										<input type="checkbox">
										<span data-toggle="tooltip" data-placement="top" title="Green" class="checkmark green"></span>
									</label></li>
								<li><label class="container-cheeck">
										<input type="checkbox">
										<span data-toggle="tooltip" data-placement="top" title="White" class="checkmark white-check"></span>
									</label>
								</li>
								<li><label class="container-cheeck">
										<input type="checkbox">
										<span data-toggle="tooltip" data-placement="top" title="Black" class="checkmark black"></span>
									</label></li>


							</ul>
							<ul class="widget-body-ul1">
								<li>  <label class="container-cheeck">
										<input type="checkbox" checked="checked">
										<span data-toggle="tooltip" data-placement="top" title="Pink" class="checkmark pink"></span>
									</label></li>
								<li><label class="container-cheeck">
										<input type="checkbox">
										<span data-toggle="tooltip" data-placement="top" title="Yellow" class="checkmark yellow"></span>
									</label></li>
								<li><label class="container-cheeck">
										<input type="checkbox">
										<span data-toggle="tooltip" data-placement="top" title="Purple" class="checkmark purple"></span>
									</label></li>
								<li><label class="container-cheeck">
										<input type="checkbox">
										<span data-toggle="tooltip" data-placement="top" title="Orange" class="checkmark orange"></span>
									</label></li>


							</ul>






						</div><!-- End .widget -->


						<div class="widget widget-block">
							<h3 class="widget-title">
								<a data-toggle="collapse" href="#widget-body-4" role="button" aria-expanded="true" aria-controls="widget-body-4">Choose type</a>
							</h3>
							<div class="filter-content">
								<div class="card-body">
									<label class="form-check">
										<input class="form-check-input" type="radio" name="exampleRadio" value="">
										<span class="form-check-label">
							    First hand items
							  </span>
									</label>
									<label class="form-check">
										<input class="form-check-input" type="radio" name="exampleRadio" value="">
										<span class="form-check-label">
							    Brand new items
							  </span>
									</label>
									<label class="form-check">
										<input class="form-check-input" type="radio" name="exampleRadio" value="">
										<span class="form-check-label">
							    Some other option
							  </span>
									</label>
								</div> <!-- card-body.// -->
							</div>
						</div><!-- End .widget -->
					</div><!-- End .sidebar-wrapper -->
				</aside><!-- End .col-lg-3 -->

				<div class="col-sm-12 col-md-9 col-lg-9">
					<div class="row">

						<div class="col-sm-12 col-md-12 col-lg-12">
							<h2 class="section-heading text-center">
								Hello! Welcome to Best Life Ltd. Business
							</h2>
							<p class="subheading text-center mb-5">Awesome features we offer exclusive only</p>
						</div>
						<!-- Item 1 -->
						<div class="col-sm-12 col-md-6 col-lg-4 mb-4">
							<div class="rs-box-project">
								<div class="media-box">
									<a href="<?php echo base_url(); ?>ui/frontend/images/600x500-6.jpg" title="Gallery #1">
										<img src="<?php echo base_url(); ?>ui/frontend/images/600x500-6.jpg" alt="" class="img-fluid">
										<div class="project-info">
											<div class="project-icon">
												<span class="fa fa-search"></span>
											</div>
										</div>
									</a>
								</div>
								<div class="body">
									<div class="title">Notebook Mockup</div>
									<div class="category">Design</div>

								</div>
							</div>
						</div>
						<!-- Item 2 -->
						<div class="col-sm-12 col-md-6 col-lg-4 mb-4">
							<div class="rs-box-project">
								<div class="media-box">
									<a href="<?php echo base_url(); ?>ui/frontend/images/600x500-5.jpg" title="Gallery #2">
										<img src="<?php echo base_url(); ?>ui/frontend/images/600x500-5.jpg" alt="" class="img-fluid">
										<div class="project-info">
											<div class="project-icon">
												<span class="fa fa-search"></span>
											</div>
										</div>
									</a>
								</div>
								<div class="body">
									<div class="title">Paper Hot Cup</div>
									<div class="category">Design</div>

								</div>
							</div>
						</div>
						<!-- Item 3 -->
						<div class="col-sm-12 col-md-6 col-lg-4 mb-4">
							<div class="rs-box-project">
								<div class="media-box">
									<a href="<?php echo base_url(); ?>ui/frontend/images/600x500-.jpg" title="Gallery #3">
										<img src="<?php echo base_url(); ?>ui/frontend/images/600x500-.jpg" alt="" class="img-fluid">
										<div class="project-info">
											<div class="project-icon">
												<span class="fa fa-search"></span>
											</div>
										</div>
									</a>
								</div>
								<div class="body">
									<div class="title">Jamu Bottle</div>
									<div class="category">Design</div>

								</div>
							</div>
						</div>
						<!-- Item 4 -->
						<div class="col-sm-12 col-md-6 col-lg-4 mb-4">
							<div class="rs-box-project">
								<div class="media-box">
									<a href="<?php echo base_url(); ?>ui/frontend/images/600x500-2.jpg" title="Gallery #4">
										<img src="<?php echo base_url(); ?>ui/frontend/images/600x500-2.jpg" alt="" class="img-fluid">
										<div class="project-info">
											<div class="project-icon">
												<span class="fa fa-search"></span>
											</div>
										</div>
									</a>
								</div>
								<div class="body">
									<div class="title">Papper Bag</div>
									<div class="category">Design</div>

								</div>
							</div>
						</div>
						<!-- Item 5 -->
						<div class="col-sm-12 col-md-6 col-lg-4 mb-4">
							<div class="rs-box-project">
								<div class="media-box">
									<a href="<?php echo base_url(); ?>ui/frontend/images/600x500-3.jpg" title="Gallery #5">
										<img src="<?php echo base_url(); ?>ui/frontend/images/600x500-3.jpg" alt="" class="img-fluid">
										<div class="project-info">
											<div class="project-icon">
												<span class="fa fa-search"></span>
											</div>
										</div>
									</a>
								</div>
								<div class="body">
									<div class="title">Hanging T-Shirt</div>
									<div class="category">Design</div>

								</div>
							</div>
						</div>
						<!-- Item 6 -->
						<div class="col-sm-12 col-md-6 col-lg-4 mb-4">
							<div class="rs-box-project">
								<div class="media-box">
									<a href="<?php echo base_url(); ?>ui/frontend/images/600x500-4.jpg" title="Gallery #6">
										<img src="<?php echo base_url(); ?>ui/frontend/images/600x500-4.jpg" alt="" class="img-fluid">
										<div class="project-info">
											<div class="project-icon">
												<span class="fa fa-search"></span>
											</div>
										</div>
									</a>
								</div>
								<div class="body">
									<div class="title">Notebook Mockup Resource</div>
									<div class="category">Design</div>

								</div>
							</div>
						</div>
					</div>
				</div>


			</div>
		</div>
	</div>
</div>


<!-- CTA -->
<div class="section bg-primary">
	<div class="content-wrap py-5">
		<div class="container">

			<div class="row align-items-center">
				<div class="col-sm-12 col-md-12">
					<div class="cta-1">
						<div class="body-text text-white mb-3">
							<h3 class="my-1">Grow Up Your Business With Best Life Ltd.</h3>
							<p class="uk18 mb-0">We provide high standar clean website for your business solutions</p>
						</div>
						<div class="body-action mt-3">
							<a href="#" class="btn btn-secondary">PURCHASE NOW</a>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>


<?php
$this->load->view('frontend/layout/footer');
?>
