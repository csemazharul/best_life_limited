<?php
$this->load->view('frontend/layout/header');
?>

<!-- BANNER -->
<div class="section banner-page" data-background="<?php echo base_url(); ?>ui/frontend/images/dummy-img-1920x300.jpg">
	<div class="content-wrap pos-relative">
		<div class="d-flex justify-content-center bd-highlight mb-3">
			<div class="title-page">ABOUT</div>
		</div>
		<div class="d-flex justify-content-center bd-highlight mb-3">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb ">
					<li class="breadcrumb-item"><a href="index.html">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page">About</li>
				</ol>
			</nav>
		</div>
	</div>
</div>

<!-- WHO WE ARE -->
<div class="section">
	<div class="content-wrap">
		<div class="container">

			<div class="row">
				<div class="col-sm-12 col-md-12 col-lg-6">
					<h2 class="section-heading text-left">
						WHO WE ARE?
					</h2>

					<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium dolore mque laudantium, totam rem aperiam, eaque ipsa quae ab illo invent.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
					<a href="#" class="btn btn-primary">READ MORE</a>
					<div class="spacer-30"></div>
				</div>
				<div class="col-sm-12 col-md-12 col-lg-6">

					<div id="whoweare" class="whoweare owl-carousel owl-theme" data-background="<?php echo base_url(); ?>ui/frontend/images/laptop.png">
						<!-- Item 1 -->
						<div class="item">
							<img src="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x400.jpg" alt="">
						</div>
						<!-- Item 2 -->
						<div class="item">
							<img src="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x400.jpg" alt="">
						</div>
						<!-- Item 3 -->
						<div class="item">
							<img src="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x400.jpg" alt="">
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>

<!-- FUN FACT -->
<div class="section bg-secondary">
	<div class="content-wrap">
		<div class="container">

			<div class="row">

				<!-- Item 1 -->
				<div class="col-sm-6 col-md-6 col-lg-3">
					<div class="rs-icon-funfact-2">
						<div class="icon">
							<i class="fa fa-briefcase"></i>
						</div>
						<div class="body-content">
							<h2>1200</h2>
							<p>PROJECTS</p>
						</div>
					</div>
				</div>

				<!-- Item 2 -->
				<div class="col-sm-6 col-md-6 col-lg-3">
					<div class="rs-icon-funfact-2">
						<div class="icon">
							<i class="fa fa-thumbs-up"></i>
						</div>
						<div class="body-content">
							<h2>25000</h2>
							<p>FOLLOW & LIKE</p>
						</div>
					</div>
				</div>

				<!-- Item 3 -->
				<div class="col-sm-6 col-md-6 col-lg-3">
					<div class="rs-icon-funfact-2">
						<div class="icon">
							<i class="fa fa-coffee"></i>
						</div>
						<div class="body-content">
							<h2>6000</h2>
							<p>CUP OF COFFEE</p>
						</div>
					</div>
				</div>

				<!-- Item 4 -->
				<div class="col-sm-6 col-md-6 col-lg-3">
					<div class="rs-icon-funfact-2">
						<div class="icon">
							<i class="fa fa-users"></i>
						</div>
						<div class="body-content">
							<h2>800</h2>
							<p>CLIENTS</p>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

<!-- WELCOME TO DAZE -->
<div id="about" class="section">
	<div class="content-wrap">
		<div class="container">

			<div class="row">

				<div class="col-sm-12 col-md-12 col-lg-4">
					<h2 class="section-heading text-left">
						AWESOME SKILLS
					</h2>

					<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium dolore mque laudantium, totam rem aperiam, eaque ipsa quae ab illo invent.</p>
					<div class="rs-progress mb-3">
						<div class="name">Development</div>
						<div class="persen">80%</div>
						<div class="progress">
							<div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
					</div>
					<div class="rs-progress mb-3">
						<div class="name">HTML</div>
						<div class="persen">90%</div>
						<div class="progress">
							<div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
					</div>
					<div class="rs-progress mb-3">
						<div class="name">Marketing</div>
						<div class="persen">70%</div>
						<div class="progress">
							<div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
					</div>
					<div class="rs-progress mb-3">
						<div class="name">Adobe Apps</div>
						<div class="persen">80%</div>
						<div class="progress">
							<div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
					</div>


				</div>

				<div class="col-sm-12 col-md-12 col-lg-8">

					<img src="<?php echo base_url(); ?>ui/frontend/images/dummy-img-900x600.jpg" alt="" class="img-fluid shadow-lg mb-3">

				</div>

			</div>

		</div>
	</div>
</div>

<!-- MEET THE TEAM -->
<div class="section bg-gray-light">
	<div class="content-wrap">
		<div class="container">

			<div class="row mb-5">
				<div class="col-sm-12 col-md-12">
					<h2 class="section-heading text-center no-after mb-3">
						MEET THE TEAM
					</h2>

				</div>
			</div>

			<div class="row">

				<!-- Item 1 -->
				<div class="col-sm-12 col-md-6 col-lg-3">
					<div class="rs-team-1">

						<div class="media shadow"><img src="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x600.jpg" alt="" class="img-fluid"></div>
						<div class="body">
							<div class="title">Yahdi Romelo</div>
							<div class="position">CEO & Founder Company</div>
							<ul class="sosmed-icon d-inline-flex mb-3">
								<li><a href="#"><span class="fa fa-facebook"></span></a></li>
								<li><a href="#"><span class="fa fa-twitter"></span></a></li>
								<li><a href="#"><span class="fa fa-skype"></span></a></li>
								<li><a href="#"><span class="fa fa-linkedin"></span></a></li>
							</ul>
						</div>
					</div>
				</div>

				<!-- Item 2 -->
				<div class="col-sm-12 col-md-6 col-lg-3">
					<div class="rs-team-1">

						<div class="media shadow"><img src="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x600.jpg" alt="" class="img-fluid"></div>
						<div class="body">
							<div class="title">Rudhi Sas</div>
							<div class="position">Web Designer</div>
							<ul class="sosmed-icon d-inline-flex mb-3">
								<li><a href="#"><span class="fa fa-facebook"></span></a></li>
								<li><a href="#"><span class="fa fa-twitter"></span></a></li>
								<li><a href="#"><span class="fa fa-skype"></span></a></li>
								<li><a href="#"><span class="fa fa-linkedin"></span></a></li>
							</ul>
						</div>
					</div>
				</div>

				<!-- Item 3 -->
				<div class="col-sm-12 col-md-6 col-lg-3">
					<div class="rs-team-1">

						<div class="media shadow"><img src="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x600.jpg" alt="" class="img-fluid"></div>
						<div class="body">
							<div class="title">Oliver Doel</div>
							<div class="position">Graphic Designer</div>
							<ul class="sosmed-icon d-inline-flex mb-3">
								<li><a href="#"><span class="fa fa-facebook"></span></a></li>
								<li><a href="#"><span class="fa fa-twitter"></span></a></li>
								<li><a href="#"><span class="fa fa-skype"></span></a></li>
								<li><a href="#"><span class="fa fa-linkedin"></span></a></li>
							</ul>
						</div>
					</div>
				</div>

				<!-- Item 4 -->
				<div class="col-sm-12 col-md-6 col-lg-3">
					<div class="rs-team-1">

						<div class="media shadow"><img src="<?php echo base_url(); ?>ui/frontend/images/dummy-img-600x600.jpg" alt="" class="img-fluid"></div>
						<div class="body">
							<div class="title">Sahsa Doel</div>
							<div class="position">Marketing & Publiser</div>
							<ul class="sosmed-icon d-inline-flex mb-3">
								<li><a href="#"><span class="fa fa-facebook"></span></a></li>
								<li><a href="#"><span class="fa fa-twitter"></span></a></li>
								<li><a href="#"><span class="fa fa-skype"></span></a></li>
								<li><a href="#"><span class="fa fa-linkedin"></span></a></li>
							</ul>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

<!-- ABOUT -->
<div class="section">
	<div class="content-wrap">
		<div class="container">
			<div class="row">

				<div class="col-sm-12 col-md-12 col-lg-6">

					<h3 class="text-black">WHY CHOOSE US?</h3>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam, odioserunt provident maiores consectetur adipisicing elit. Aliquam odio dese runtesseu provident maiores libero porro dolorem est. Velit necessitatibus fugiat error incidunt excepturi doloribus.</p>

					<p class="p-check">Aenean vitae quam. Vivamus et nunc nunc conseq</p>
					<p class="p-check">Donec facilisis velit eu est phasellus consequat quis nostrud</p>
					<p class="p-check">Sem vel metus imperdiet lacinia enea sapiente maior</p>
					<p class="p-check">Dapibus aliquam augue fusce tels optio facilis sapiente maiores</p>

					<div class="spacer-30"></div>

				</div>
				<div class="col-sm-12 col-md-12 col-lg-6">

					<div class="accordion rs-accordion" id="accordionExample">
						<!-- Item 1 -->
						<div class="card">
							<div class="card-header" id="headingOne">
								<h3 class="title">
									<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
										Clean & Profesional Design
									</button>
								</h3>
							</div>
							<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
								<div class="card-body">
									Create and publilsh dynamic websites for desktop, tablet, and mobile devices that meet the latest web standards- without writing code. Design freely using familiar tools and hundreds of web fonts. easily add interactivity, including slide shows, forms, and more.
								</div>
							</div>
						</div>
						<!-- Item 2 -->
						<div class="card">
							<div class="card-header" id="headingTwo">
								<h3 class="title">
									<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
										Top Week Selling on Themeforest
									</button>
								</h3>
							</div>
							<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
								<div class="card-body">
									Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
								</div>
							</div>
						</div>
						<!-- Item 3 -->
						<div class="card">
							<div class="card-header" id="headingThree">
								<h3 class="title">
									<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
										Responsive Layout
									</button>
								</h3>
							</div>
							<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
								<div class="card-body">
									<p>Unzip the file, locate Html file and double click the file and you will directly to adobe Html. Next step you can modifications our template, you can customize color, text, font, content, logo and image with your need using familiar tools on adobe Html without writing any code.</p>
									<p>You can't re-distribute the Item as stock, in a tool or template, or with source files. You can't re-distribute or make available the Item as-is or with superficial modifications. These things are not allowed even if the re-distribution is for Free.</p>
								</div>
							</div>
						</div>
					</div>
					<!-- end accordion -->

				</div>

			</div>
		</div>
	</div>
</div>

<!-- OUR TESTIMONIALS -->
<div class="section bgi-cover-center" data-background="<?php echo base_url(); ?>ui/frontend/images/dummy-img-1920x900-3.jpg">
	<div class="content-wrap">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<h2 class="section-heading text-center text-white mb-5">
						CLIENTS SAY
					</h2>
				</div>
			</div>
			<div class="row">

				<div class="col-sm-12 col-md-12 col-lg-8 offset-lg-2">
					<div id="testimonial" class="owl-carousel owl-theme owl-light">
						<!-- Item 1 -->
						<div class="item">
							<div class="rs-testimonial-3">
								<div class="quote-box">
									<blockquote>
										Teritatis et quasi architecto. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium dolore mque laudantium, totam rem aperiam
									</blockquote>
									<div class="media">
										<img src="<?php echo base_url(); ?>ui/frontend/images/dummy-img-400x400.jpg" alt="" class="rounded-circle">
									</div>
									<p class="quote-name">
										Johnathan Doel <span>CEO Rometheme</span>
									</p>
								</div>
							</div>
						</div>
						<!-- Item 2 -->
						<div class="item">
							<div class="rs-testimonial-3">
								<div class="quote-box">
									<blockquote>
										Teritatis et quasi architecto. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium dolore mque laudantium, totam rem aperiam
									</blockquote>
									<div class="media">
										<img src="<?php echo base_url(); ?>ui/frontend/images/dummy-img-400x400.jpg" alt="" class="rounded-circle">
									</div>
									<p class="quote-name">
										Linda Doel <span>CEO Bukakreasi</span>
									</p>
								</div>
							</div>
						</div>
						<!-- Item 3 -->
						<div class="item">
							<div class="rs-testimonial-3">
								<div class="quote-box">
									<blockquote>
										Teritatis et quasi architecto. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium dolore mque laudantium, totam rem aperiam
									</blockquote>
									<div class="media">
										<img src="<?php echo base_url(); ?>ui/frontend/images/dummy-img-400x400.jpg" alt="" class="rounded-circle">
									</div>
									<p class="quote-name">
										Johny Doel <span>CEO abc.xyz</span>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

<!-- CTA -->
<div class="section bg-primary">
	<div class="content-wrap py-5">
		<div class="container">

			<div class="row align-items-center">
				<div class="col-sm-12 col-md-12">
					<div class="cta-1">
						<div class="body-text text-white mb-3">
							<h3 class="my-1">Grow Up Your Business With Coxe</h3>
							<p class="uk18 mb-0">We provide high standar clean website for your business solutions</p>
						</div>
						<div class="body-action mt-3">
							<a href="#" class="btn btn-secondary">PURCHASE NOW</a>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>


<?php
$this->load->view('frontend/layout/footer');
?>
