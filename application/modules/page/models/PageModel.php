<?php


class PageModel extends MX_Controller
{
	function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public function pageInsert($page){


		$this->db->insert('pages', $page);

	}
	public function getAllPages()
	{
		$query = $this->db->get('pages');
		return $query->result();
	}

	public function getPage($id)
	{
		$query = $this->db->get_where('pages',array('id'=>$id));
		return $query->row_array();
	}

	public function pageUpdate($page, $id){
		$this->db->where('pages.id', $id);
		return $this->db->update('pages', $page);
	}

	public function deletePage($id)
	{
		$this->db->where('pages.id', $id);
		return $this->db->delete('pages');
	}



}
