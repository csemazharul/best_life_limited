<?php


class Page extends MX_Controller
{
	public function __construct()
	{

		parent::__construct();
		$this->load->helper('url');
		$this->load->model('PageModel');
		$this->load->model('subcategory/SubCategoryModel');
		$this->load->model('slider/SliderModel');
		$this->load->model('category/CategoryModel');
		$this->load->library('session');

	}

	public function home()
	{
		$slider['sliders']=$this->SliderModel->getAllSliders();


		$this->load->view('/index',$slider);
	}

	public function about()
	{
		$this->load->view('/about');
	}

	public function services()
	{
		$this->load->view('/services');
	}

	public function portfolio()
	{

		$this->load->view('/portfolio');
	}

	public function contact()
	{
		$this->load->view('/contact');

	}


	public function products()
	{
		$category['categories']=$this->CategoryModel->getAllCategories();


		$this->load->view('/our_product',$category);

	}

	public function admin()
	{
		if(isset($_SESSION['user_email']))
		{
			$this->load->view('backend/index');


		}
		else
		{
			redirect('/login');

		}
	}

	public function pageCreate()
	{
		$this->load->view('page/page/create.php');
	}
	public function pageList()
	{
		$page['pages']=$this->PageModel->getAllPages();

		$this->load->view('page/page/list.php',$page);
	}

	public function pageStore()
	{
		if(!empty($_FILES['photo']['name'])){
			$config['upload_path'] = 'upload/images/';
			$config['allowed_types'] = 'jpg|jpeg|png|gif';
			$config['file_name'] = $_FILES['photo']['name'];

			//Load upload library and initialize configuration
			$this->load->library('upload',$config);
			$this->upload->initialize($config);

			if($this->upload->do_upload('photo')){
				$uploadData = $this->upload->data();
				$picture = $uploadData['file_name'];

			}else{
				$picture = '';
			}
		}else{
			$picture = '';
		}


		$page['title'] = $this->input->post('title');
		$page['description'] = $this->input->post('description');
		$page['name'] = $this->input->post('name');
		$page['photo'] = $picture;
		$page['page_name'] = $this->input->post('page_name');
		$page['created_at'] =date('Y-m-d h:i:a');
		$page['updated_at'] =date('Y-m-d h:i:a');

		$query = $this->PageModel->pageInsert($page);

		redirect('page/list','refresh');

	}

	public function pageEdit()
	{
		$id = $this->uri->segment(3);

		$page['page'] = $this->PageModel->getPage($id);

		$this->load->view('page/page/edit',$page);
	}

	public function pageUpdate()
	{

		$id = $this->uri->segment(3);
		$page= $this->PageModel->getPage($id);
		$create_at=$page['created_at'];

		if(!empty($_FILES['photo']['name'])){
			$config['upload_path'] = 'upload/images/';
			$config['allowed_types'] = 'jpg|jpeg|png|gif';
			$config['file_name'] = $_FILES['photo']['name'];

			//Load upload library and initialize configuration
			$this->load->library('upload',$config);
			$this->upload->initialize($config);

			if($this->upload->do_upload('photo')){
				$uploadData = $this->upload->data();
				$picture = $uploadData['file_name'];

			}else{
				$picture ='';
			}
		}else{
			$picture =  $page['photo'];
		}

		$page['title'] = $this->input->post('title');
		$page['description'] = $this->input->post('description');
		$page['name'] = $this->input->post('name');
		$page['photo'] = $picture;
		$page['page_name'] = $this->input->post('page_name');
		$page['created_at'] = $create_at;
		$page['updated_at'] =date('Y-m-d h:i:a');
		$query = $this->PageModel->pageUpdate($page,$id);

		redirect('page/list','refresh');

	}

	public function delete(){
		$id = $this->uri->segment(3);

		$query = $this->PageModel->deletePage($id);

		redirect('page/list','refresh');
	}

}
