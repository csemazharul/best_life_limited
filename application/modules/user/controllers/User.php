<?php


class User extends MX_Controller
{
	public function __construct(){

		parent::__construct();

		$this->load->helper(array('form', 'url'));
		$this->load->model('user_model');
		$this->load->library('session');
		$this->load->library('form_validation');
	}

	public function register()
	{
		if(isset($_SESSION['user_email']))
		{
			redirect('admin');

		}
		else
		{
			$this->load->view('/register');

		}

	}

	public function register_user()
	{


		$this->form_validation->set_rules('user_name', 'Name', 'required');
		$this->form_validation->set_rules('user_email', 'Email ID', 'required');
		$this->form_validation->set_rules('user_password', 'Password', 'required');
		$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[user_password]');

		if ($this->form_validation->run() == FALSE)
		{

			$this->load->view("register.php");
		}
		else
		{
			$user=array(
				'user_name'=>$this->input->post('user_name'),
				'user_email'=>$this->input->post('user_email'),
				'user_password'=>md5($this->input->post('user_password')),
				'email_verified'=>md5(rand(1,100000)),
			);
			$this->user_model->register_user($user);
			$this->sendEmail($user['email_verified'],$user['user_email']);
			$this->session->set_flashdata('message', '<strong>Success!</strong> Email has been sent successfully.</span>');
			$this->load->view('register');

		}
	}

	public function sendEmail($token,$email)
	{

		$this->load->library('email');
		$config = array();
		$config['protocol'] = 'smtp';
		$config['smtp_host'] = 'smtp.mailgun.org';
		$config['smtp_user'] = 'postmaster@test.myscoresme.com';
		$config['smtp_pass'] = '20c4b1191b6822c9ac9655d9d0bb1905-41a2adb4-7f832e5d';
		$config['smtp_port'] = 587;
		$this->email->initialize($config);
		$this->email->set_newline("\r\n");

		$this->email->from('postmaster@test.myscoresme.com', 'Best Life Limited');
		$this->email->to($email);
		$this->email->subject('Email Verification');
		$message = "
       Please click this link to verify your account:
       http://best_life_limited.test/verified/".$token;

		$this->email->message($message);

		$this->email->send();

	}
	public function verified()
	{
		$verified = $this->uri->segment(2);
		$check=$this->user_model->verified_check($verified);
		if($check)
		{
			$this->db->set('email_verified',1); //value that used to update column
			$this->db->where('email_verified', $verified); //which row want to upgrade
			$status=$this->db->update('user');  //table name
			if($status)
			{
				$this->session->set_flashdata('message', '<strong>Success!</strong>Your email verified..please login now.</span>');
				redirect('/login');
			}
		}
	}

	public function loginForm(){
		if(isset($_SESSION['user_email']))
		{
			redirect('admin');


		}
		else
		{
			$this->load->view('/login');

		}

	}

	function login_user(){
		$user_login=array(

			'user_email'=>$this->input->post('user_email'),
			'user_password'=>md5($this->input->post('user_password'))

		);
//$user_login['user_email'],$user_login['user_password']
		$data['users']=$this->user_model->login_user($user_login['user_email'],$user_login['user_password']);
		if($data['users'])
		{
			$this->session->set_userdata('user_email',$data['users'][0]['user_email']);
			$this->session->set_userdata('user_name',$data['users'][0]['user_name']);
			redirect('/admin', 'refresh');

		}
		else{
			$this->session->set_flashdata('message', 'Wrong Information please again try again.');
			$this->load->view("/login.php");

		}


	}

	public function user_logout(){

		$this->session->sess_destroy();
		$this->load->view("/login.php");
	}

}
