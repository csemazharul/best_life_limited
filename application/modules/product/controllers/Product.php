<?php


class Product extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('ProductModel');
		$this->load->model('subcategory/SubCategoryModel');
		$this->load->model('category/CategoryModel');
		$this->load->library('session');

	}

	public function productCreate()
	{
		$columns = '`id`,`title`';
		$this->db->select($columns, FALSE);
	   $query=$this->db->get('categories');
	   $category['categories']=$query->result();

		$this->load->view('product/create.php',$category);
	}

	public function findSubcategory()
	{
		$id = $this->uri->segment(2);
		$this->db->select('*');

		$this->db->from('categories');
		$this->db->where('categories.id',$id);
		$this->db->join('subcategories', 'categories.id=subcategories.category_id');

		$query = $this->db->get();

		echo json_encode($query->result());
	}

	public function productList()
	{
		$product['products']=$this->ProductModel->getAllSliders();

		$this->load->view('product/list.php',$product);
	}

	public function productStore()
	{
		if(!empty($_FILES['picture']['name']))
		{
			$config['upload_path'] = 'upload/images/';
			$config['allowed_types'] = 'jpg|jpeg|png|gif';
			$config['file_name'] = $_FILES['picture']['name'];

			//Load upload library and initialize configuration
			$this->load->library('upload',$config);
			$this->upload->initialize($config);

			if($this->upload->do_upload('picture'))
			{
				$uploadData = $this->upload->data();
				$picture = $uploadData['file_name'];

			}else{
				$picture = '';
			}
		}else{
			$picture = '';
		}

		$product['category_id'] = $this->input->post('category_id');
		$product['sub_id'] = $this->input->post('sub_id');
		$product['title'] = $this->input->post('title');
		$product['short_description'] = $this->input->post('short_description');
		$product['picture'] = $picture;
		$product['price'] = $this->input->post('price');
		$product['size'] = $this->input->post('size');
		$product['color'] = $this->input->post('color');


		$product['created_at'] =date('Y-m-d h:i:a');
		$product['updated_at'] =date('Y-m-d h:i:a');

		$query = $this->ProductModel->productInsert($product);

		redirect('product/list','refresh');

	}

	public function productEdit()
	{
		$id = $this->uri->segment(3);
		$product['product'] = $this->ProductModel->getProduct($id);
		$this->load->view('product/edit',$product);
	}

	public function productUpdate()
	{

		$id = $this->uri->segment(3);

		$productData= $this->ProductModel->getSlider($id);
		

		if(!empty($_FILES['picture']['name'])){
			$config['upload_path'] = 'upload/images/';
			$config['allowed_types'] = 'jpg|jpeg|png|gif';
			$config['file_name'] = $_FILES['picture']['name'];

			//Load upload library and initialize configuration
			$this->load->library('upload',$config);
			$this->upload->initialize($config);

			if($this->upload->do_upload('product')){
				$uploadData = $this->upload->data();
				$picture = $uploadData['file_name'];

			}else{
				$picture ='';
			}
		}else{
			$picture =  $productData['picture'];
		}

		$product['category_id'] = $this->input->post('category_id');
		$product['title'] = $this->input->post('title');
		$product['short_description'] = $this->input->post('short_description');
		$product['picture'] = $picture;
		$product['price'] = $this->input->post('price');
		$product['size'] = $this->input->post('size');
		$product['color'] = $this->input->post('color');
		$product['created_at']=$productData['created_at'];
		$product['updated_at']=date('Y-m-d h:i:a');
		$query = $this->ProductModel->productUpdate($product,$id);

		redirect('product/list','refresh');

	}

	public function delete()
	{
		$id = $this->uri->segment(3);

		$query = $this->ProductModel->deleteProduct($id);

		redirect('product/list','refresh');
	}


}
