<?php
$this->load->view('backend/layout/header');
?>

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header text-uppercase">Product Create</div>
					<div class="card-body">

						<form method="post" action="<?php echo base_url()?>product/store" enctype="multipart/form-data">

							<div class="form-group row">
								<label for="basic-input" class="col-sm-3 col-form-label">Title</label>
								<div class="col-sm-9">
									<input type="text" name="title" id="basic-input" class="form-control">
								</div>
							</div>

							<div class="form-group row">
								<label for="basic-input" class="col-sm-3 col-form-label">Select Your Category</label>
								<div class="col-sm-9">
									<select id="category_id" onchange="getSubcategory()" name="category_id" class="form-control">
										<option disabled selected>select your category</option>
										<?php
										foreach($categories as $category) {
											?>

											<option value="<?php echo $category->id?>"><?php echo $category->title?></option>
											<?php
										}
										?>
									</select>

								</div>
							</div>

							<div class="form-group row">
								<label for="basic-input" class="col-sm-3 col-form-label">Select Your Sub Category</label>
								<div class="col-sm-9">
									<select id="sub_id" name="sub_id" class="form-control">

									</select>

								</div>
							</div>

							<div class="form-group row">
								<label for="basic-textarea" class="col-sm-3 col-form-label">Short Description</label>
								<div class="col-sm-9">
									<textarea rows="8" name="short_description" class="form-control" id="basic-textarea"></textarea>
								</div>
							</div>

							<div class="form-group row">
								<label for="basic-input" class="col-sm-3 col-form-label">Picture</label>
								<div class="col-sm-9">
									<input type="file" name="picture" class="form-control">
								</div>
							</div>

							<div class="form-group row">
								<label for="basic-textarea" class="col-sm-3 col-form-label">price</label>
								<div class="col-sm-9">
									<input type="number" name="price" id="basic-input" class="form-control">
								</div>
							</div>

							<div class="form-group row">
								<label for="basic-textarea" class="col-sm-3 col-form-label">Size</label>
								<div class="col-sm-9">

									<select name="size" class="form-control">
										<option aria-disabled="true">Select Size</option>
										<option value="S">S</option>
										<option value="M">M</option>
										<option value="L">L</option>
										<option value="XL">XL</option>
										<option value="2XL">2XL</option>
										<option value="3XL">3XL</option>
									</select>
								</div>
							</div>

							<div class="form-group row">
								<label for="basic-textarea" class="col-sm-3 col-form-label">Color</label>
								<div class="col-sm-9">
									<input type="color" name="color" id="basic-input" class="form-control">
								</div>
							</div>

							<button type="submit" class="btn btn btn-primary shadow btn-block ">Submit</button>

						</form>

					</div>
				</div>
			</div>
		</div><!--End Row-->
	</div>
	<!-- End container-fluid-->


</div><!--End content-wrapper

<?php
$this->load->view('backend/layout/footer');
?>
