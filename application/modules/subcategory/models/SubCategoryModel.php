<?php


class SubCategoryModel extends MX_Controller
{
	function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public function subCategoryInsert($category)
	{

		$this->db->insert('subcategories', $category);

	}
	public function getAllSubCategories()
	{
		$query = $this->db->get('subcategories');
		return $query->result();
	}

	public function getCategory($id)
	{
		$query = $this->db->get_where('subcategories',array('id'=>$id));
		return $query->row_array();
	}

	public function categoryUpdate($category, $id)
	{
		$this->db->where('subcategories.id', $id);
		return $this->db->update('subcategories', $category);
	}

	public function deleteCategory($id)
	{

		$this->db->where('subcategories.id', $id);
		return $this->db->delete('subcategories');
	}


}
