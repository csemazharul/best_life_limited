<?php


class SubCategory extends MX_Controller
{
	public function __construct(){

		parent::__construct();
		$this->load->helper('url');
		$this->load->model('category/CategoryModel');
		$this->load->model('SubCategoryModel');
		$this->load->library('session');

	}

	public function subCreate()
	{

		$category['categories']=$this->CategoryModel->getAllCategories();

		$this->load->view('subcategory/create.php',$category);
	}

	public function subCategoryList()
	{

		$subCategory['subcategories']=$this->SubCategoryModel->getAllSubCategories();

		$this->load->view('subcategory/list.php',$subCategory);
	}

	public function subStore()
	{

		$subCategory['title'] = $this->input->post('title');
		$subCategory['category_id'] = $this->input->post('category_id');
		$subCategory['status'] = $this->input->post('status');

		$subCategory['created_at'] =date('Y-m-d h:i:a');
		$subCategory['updated_at'] =date('Y-m-d h:i:a');

		$query = $this->SubCategoryModel->subCategoryInsert($subCategory);


		redirect('sub/list','refresh');

	}

	public function subCategoryEdit()
	{
		$id = $this->uri->segment(3);

		$category['subcategory'] = $this->SubCategoryModel->getCategory($id);


		$this->load->view('subcategory/edit',$category);
	}

	public function categoryUpdate()
	{

		$id = $this->uri->segment(3);

		$categoryData= $this->SubCategory->getCategory($id);
		$category['title'] = $this->input->post('title');
		$category['created_at'] = $categoryData['created_at'];
		$category['updated_at'] =date('Y-m-d h:i:a');
		$query = $this->SubCategoryModel->categoryUpdate($category,$id);

		redirect('subcategory/list','refresh');

	}

	public function delete(){
		$id = $this->uri->segment(3);

		$query = $this->SubCategoryModel->deleteCategory($id);

		redirect('subcategory/list','refresh');
	}

}
