<?php


class SliderModel extends MX_Controller
{
	function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public function sliderInsert($slider){


		$this->db->insert('sliders', $slider);

	}
	public function getAllSliders()
	{
		$query = $this->db->get('sliders');
		return $query->result();
	}

	public function getSlider($id)
	{
		$query = $this->db->get_where('sliders',array('id'=>$id));
		return $query->row_array();
	}

	public function sliderUpdate($slider, $id){
		$this->db->where('sliders.id', $id);
		return $this->db->update('sliders', $slider);
	}

	public function deleteSlider($id)
	{
		$this->db->where('sliders.id', $id);
		return $this->db->delete('sliders');
	}


}
