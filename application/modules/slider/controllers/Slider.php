<?php


class Slider extends MX_Controller
{
	public function __construct(){

		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model('SliderModel');

	}

	public function sliderCreate()
	{

		$this->load->view('slider/create.php');
	}

	public function sliderList()
	{
		$slider['sliders']=$this->SliderModel->getAllSliders();

		$this->load->view('slider/list.php',$slider);
	}

	public function sliderStore()
	{
		if(!empty($_FILES['picture']['name'])){
			$config['upload_path'] = 'upload/images/';
			$config['allowed_types'] = 'jpg|jpeg|png|gif';
			$config['file_name'] = $_FILES['picture']['name'];

			//Load upload library and initialize configuration
			$this->load->library('upload',$config);
			$this->upload->initialize($config);

			if($this->upload->do_upload('picture')){
				$uploadData = $this->upload->data();
				$picture = $uploadData['file_name'];

			}else{
				$picture = '';
			}
		}else{
			$picture = '';
		}


		$slider['title'] = $this->input->post('title');
		$slider['page_id'] = $this->input->post('page_id');
		$slider['short_description'] = $this->input->post('short_description');
		$slider['picture'] = $picture;

		$slider['created_at'] =date('Y-m-d h:i:a');
		$slider['updated_at'] =date('Y-m-d h:i:a');

		$query = $this->SliderModel->sliderInsert($slider);

		redirect('slider/list','refresh');

	}

	public function sliderEdit()
	{
		$id = $this->uri->segment(3);
		$slider['slider'] = $this->SliderModel->getSlider($id);
		$this->load->view('slider/edit',$slider);
	}

	public function sliderUpdate()
	{

		$id = $this->uri->segment(3);

		$slider= $this->SliderModel->getSlider($id);

		$create_at=$slider['created_at'];

		if(!empty($_FILES['picture']['name'])){
			$config['upload_path'] = 'upload/images/';
			$config['allowed_types'] = 'jpg|jpeg|png|gif';
			$config['file_name'] = $_FILES['picture']['name'];

			//Load upload library and initialize configuration
			$this->load->library('upload',$config);
			$this->upload->initialize($config);

			if($this->upload->do_upload('slider')){
				$uploadData = $this->upload->data();
				$picture = $uploadData['file_name'];

			}else{
				$picture ='';
			}
		}else{
			$picture =  $slider['picture'];
		}

		$slider['title'] = $this->input->post('title');
		$slider['page_id'] = $this->input->post('page_id');
		$slider['short_description'] = $this->input->post('short_description');
		$slider['picture'] = $picture;
		$page['created_at'] = $create_at;
		$page['updated_at'] =date('Y-m-d h:i:a');
		$query = $this->SliderModel->sliderUpdate($slider,$id);

		redirect('slider/list','refresh');

	}

	public function delete(){
		$id = $this->uri->segment(3);

		$query = $this->SliderModel->deleteSlider($id);

		redirect('slider/list','refresh');
	}




}
