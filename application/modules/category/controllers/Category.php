<?php


class Category extends MX_Controller
{
	public function __construct(){

		parent::__construct();
		$this->load->helper('url');
		$this->load->model('subcategory/SubCategoryModel');
		$this->load->model('category/CategoryModel');
		$this->load->library('session');

	}

	public function categoryCreate()
	{

		$this->load->view('category/create.php');
	}

	public function categoryList()
	{
		$category['categories']=$this->CategoryModel->getAllCategories();

		$this->load->view('category/list.php',$category);
	}

	public function categoryStore()
	{

		$category['title'] = $this->input->post('title');
		$category['status'] = $this->input->post('status');

		$category['created_at'] =date('Y-m-d h:i:a');
		$category['updated_at'] =date('Y-m-d h:i:a');

		$query = $this->CategoryModel->categoryInsert($category);

		redirect('category/list','refresh');

	}

	public function categoryEdit()
	{
		$id = $this->uri->segment(3);

		$category['category'] = $this->CategoryModel->getCategory($id);


		$this->load->view('category/edit',$category);
	}

	public function categoryUpdate()
	{

		$id = $this->uri->segment(3);

		$categoryData= $this->CategoryModel->getCategory($id);
		$category['title'] = $this->input->post('title');
		$category['created_at'] = $categoryData['created_at'];
		$category['updated_at'] =date('Y-m-d h:i:a');
		$query = $this->CategoryModel->categoryUpdate($category,$id);

		redirect('category/list','refresh');

	}

	public function delete(){
		$id = $this->uri->segment(3);

		$query = $this->CategoryModel->deleteCategory($id);

		redirect('category/list','refresh');
	}

}
